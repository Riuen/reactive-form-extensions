import {Component, ElementRef, ViewChild} from "@angular/core";
import {ComponentFixture, TestBed} from "@angular/core/testing";
import {FormControl, FormGroup, ReactiveFormsModule} from "@angular/forms";
import {RFX_CONFIG} from "../../config/rfx-config";
import {appendFormControlError} from "../../utils/rfx-utils";
import {TrimDirective} from './trim.directive';

@Component({
  template: `
  <br/>
  <form [formGroup]="form">
    <input type="text" formControlName="f1" trim #f1 />
    <input type="text" formControlName="f2" trim #f2 setEmptyToNull="false"/>
    <input type="text" formControlName="f3" trim #f3/>
    <input type="date" formControlName="f4" trim #f4>

  </form>
  `
})
class TrimDirectiveTestComponent {
  @ViewChild('f1') f1!: ElementRef<HTMLInputElement>;
  @ViewChild('f2') f2!: ElementRef<HTMLInputElement>;
  @ViewChild('f3') f3!: ElementRef<HTMLInputElement>;
  @ViewChild('f4') f4!: ElementRef<HTMLInputElement>;

  form = new FormGroup({
    f1: new FormControl(null),
    f2: new FormControl(null),
    f3: new FormControl(null),
    f4: new FormControl('2018-07-23')
  });
}

describe('Trim Extension', () => {
  let component: TrimDirectiveTestComponent;
  let fixture: ComponentFixture<TrimDirectiveTestComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [TrimDirectiveTestComponent],
      imports: [
        ReactiveFormsModule,
        TrimDirective
      ],
      providers: [{provide: RFX_CONFIG, useValue: {
        trim: { setEmptyValuesToNull: true }
      }}]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TrimDirectiveTestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should trim leading and trailing spaces of string', () => {
    component.form.get('f1').setValue('    helloworld    ');
    component.f1.nativeElement.dispatchEvent(new Event('blur'));
    expect(component.form.get('f1').value).toEqual('helloworld');
  });

  it('should not attempt to trim non strings', () => {
    component.form.get('f1').setValue(['hello', 'world']);
    component.f1.nativeElement.dispatchEvent(new Event('blur'));
    expect(component.form.get('f1').value).toEqual(['hello', 'world']);
  });

  it('should not attempt to trim dates', () => {
    const now = new Date();
    component.form.get('f1').setValue(now);
    component.f1.nativeElement.dispatchEvent(new Event('blur'));
    expect(component.form.get('f1').value).toEqual(now);
  });

  it('should not attempt to trim empty objects', () => {
    component.form.get('f1').setValue({});
    component.f1.nativeElement.dispatchEvent(new Event('blur'));
    expect(component.form.get('f1').value).toEqual({});
  });

  it('should set empty values to null', () => {
    component.form.get('f1').setValue('         ');
    component.f1.nativeElement.dispatchEvent(new Event('blur'));
    expect(component.form.get('f1').value).toBeNull();
  });

  it('should override global config with attribute added directly to component', () => {
    component.form.get('f2').setValue('    ');
    component.f2.nativeElement.dispatchEvent(new Event('blur'));
    expect(component.form.get('f2').value).toEqual('')
  });

  it('should preserve the field validation error if trim operation is performed ', () => {
    component.form.get('f3').setValue('  abcdefghijklmnop  ');
    appendFormControlError(component.form.get('f3'), 'testError', 'Test error');
    component.f3.nativeElement.dispatchEvent(new Event('blur'));
    expect(component.form.get('f3').hasError('testError')).toBeTrue();
  });

  it('should not trim date fields', () => {
    component.form.get('f4').setValue('  2022-04-06  ');
    component.f4.nativeElement.dispatchEvent(new Event('blur'));
    expect(component.form.get('f4').value).toEqual('  2022-04-06  ');
  });

  it('should not trim null values', () => {
    component.form.get('f4').setValue(null);
    component.f4.nativeElement.dispatchEvent(new Event('blur'));
    expect(component.form.get('f4').value).toBeNull();
  });

});

