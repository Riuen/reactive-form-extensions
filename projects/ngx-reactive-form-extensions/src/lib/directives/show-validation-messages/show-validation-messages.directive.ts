import {ComponentRef, DestroyRef, Directive, ElementRef, inject, Input, OnInit, Optional, ViewContainerRef} from '@angular/core';
import {NgControl} from '@angular/forms';
import {combineLatest, debounceTime, distinctUntilChanged, filter, map, Observable, startWith, tap} from 'rxjs';
import {FieldValidationMessageViewComponent} from '../../components/field-validation-message-view/field-validation-message-view.component';
import {RfxConfigService} from '../../rfx-config.service';
import {extractValidationMessageFromControl, watchElementClassChange} from '../../utils/utilities';
import {takeUntilDestroyed} from '@angular/core/rxjs-interop';

/**
 * This directive extracts a reactive form's validation message and displays it below the field
 */

@Directive({
  selector: '[showValidationMessages]',
  standalone: true
})
export class ShowValidationMessagesDirective implements OnInit {

  private _errorMap = new Map();
  private _destroyRef = inject(DestroyRef);
  private _validationMsgViewComponentRef: ComponentRef<FieldValidationMessageViewComponent>;

  @Input() validationMessageClass: string;
  @Input() validationTriggerCondition: 'touched' | 'dirty';

  constructor(
    private _configSvc: RfxConfigService,
    private _elementRef: ElementRef,
    private _viewContainerRef: ViewContainerRef,
    @Optional() private _fieldControl: NgControl) {
    this.validationMessageClass = this._configSvc.getConfig().showValidationMessages.cssClass;
    this.validationTriggerCondition = this._configSvc.getConfig().showValidationMessages.validationTriggerCondition;
  }

  ngOnInit(): void {
    if (!this._fieldControl) {
      return;
    }
    this.createErrorComponent();
    combineLatest([
      this.getValidityChangeStream(),
      this.getFormResetEventStream(),
      this.getValueChangeStream()
    ]).pipe(takeUntilDestroyed(this._destroyRef))
      .pipe(debounceTime(300))
      .pipe(filter(() => this.isControlInValidationTriggerState()))
      .pipe(map(() => this.getErrorMessage()))
      .pipe(distinctUntilChanged())
      .subscribe((errorMsg) => this.updateErrorMessageComponent(errorMsg));
  }


  /*
    Creates the component that displays the error message. The component will be
    injected directly below the host.
  */
  private createErrorComponent(): void {
    this._validationMsgViewComponentRef = this._viewContainerRef.createComponent(FieldValidationMessageViewComponent);
    this._validationMsgViewComponentRef.instance.fieldClass = this.validationMessageClass;
  }

  // Utils
  private getErrorMessage(): string {
    let message = null;
    if (this._fieldControl.errors) {
      const errorName = Object.keys(this._fieldControl.errors)[0];
      if (this._errorMap.has(errorName)) {
        message = this._errorMap.get(errorName);
      } else {
        message = extractValidationMessageFromControl(errorName, this._fieldControl);
        this._errorMap.set(errorName, message);
      }
    }
    return message;
  }

  private isControlInValidationTriggerState(): boolean {
    if (!this._fieldControl.pristine || this._fieldControl.value ) {
      return true;
    }
    return this._fieldControl[this.validationTriggerCondition];
  }

  private updateErrorMessageComponent(message: string): void {
    this._validationMsgViewComponentRef.instance.message = message;
    this._validationMsgViewComponentRef.instance.detectChanges();
  }

  private getFormResetEventStream(): Observable<any> {
    return watchElementClassChange(this._elementRef)
      .pipe(filter(() => this._fieldControl.pristine))
      .pipe(distinctUntilChanged())
      .pipe(tap(() => this.updateErrorMessageComponent(null)))
      .pipe(startWith(''));
  }

  private getValidityChangeStream(): Observable<any> {
    return watchElementClassChange(this._elementRef)
      .pipe(distinctUntilChanged())
      .pipe(filter((className) => className.includes('ng-valid') || className.includes('ng-invalid')))
      .pipe(startWith(''));
  }

  private getValueChangeStream(): Observable<any> {
    return this._fieldControl.valueChanges
      .pipe(distinctUntilChanged())
      .pipe(startWith(''))
  }
}
