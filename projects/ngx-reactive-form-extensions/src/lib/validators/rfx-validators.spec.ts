import { TestBed } from "@angular/core/testing";
import { FormControl, FormGroup, ReactiveFormsModule } from "@angular/forms";
import { RfxValidators } from "./rfx-validators";

describe('Field Match Validator', () => {

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        ReactiveFormsModule
      ]
    });
  });

  it('should contain "fieldMatch" error if the field values do not match', () => {
    const form = new FormGroup({
      f1: new FormControl(null),
      f2: new FormControl(null)
    });
    form.addValidators(RfxValidators.fieldMatch('f1', 'f2', 'The values do not match'));
    form.setValue({ f1: 'v1', f2: 'v2' });
    expect(form.invalid).toBeTrue();
  });

  it('should not contain "fieldMatch" error if the field values match', () => {
    const form = new FormGroup({
      f1: new FormControl(null),
      f2: new FormControl(null)
    });
    form.addValidators(RfxValidators.fieldMatch('f1', 'f2', 'The values do not match'));
    form.setValue({ f1: 'v1', f2: 'v1' });
    expect(form.valid).toBeTrue();
  });

  it('should contain "fieldMatch" error if one field has a value and the other is null', () => {
    const form = new FormGroup({
      f1: new FormControl(null),
      f2: new FormControl(null)
    });
    form.addValidators(RfxValidators.fieldMatch('f1', 'f2', 'The values do not match'));
    form.setValue({ f1: 'v1', f2: null });
    expect(form.valid).toBeTrue();
  });
});

describe('Date Before Validator', () => {

  const ctrl = new FormControl(null, RfxValidators.dateBefore('2022/01/01'));
  const ctrl2 = new FormControl(null, RfxValidators.dateBefore('2022/01/01', 'User defined message test'));
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        ReactiveFormsModule
      ]
    });
  });

  it('should return an error if the input date exceeds the specified date.', () => {
    ctrl.setValue('2023/01/01');
    expect(ctrl.hasError('dateBefore')).toBeTrue();
  });

  it('should not contain dateBefore error if the input date is less than specified date.', () => {
    ctrl.setValue('2010/01/01');
    expect(ctrl.hasError('dateBefore')).toBeFalse();
  });

  it('should contain default error message if the input date exceeds the specified date and error message has not been overriden.', () => {
    ctrl.setValue('2023/01/01');
    const prettifiedMaxDate = new Date('2022/01/01').toLocaleDateString(undefined, { year: 'numeric', month: 'long', day: 'numeric' });
    expect(ctrl.getError('dateBefore')).toEqual(`Date entered must be less than ${prettifiedMaxDate}.`);
  });

  it('should contain user supplied error message if the input date exceeds the specified date.', () => {
    ctrl2.setValue('2023/01/01');
    expect(ctrl2.getError('dateBefore')).toEqual('User defined message test');
  });

  it('should return an error if the input date is the same as the specified date.', () => {
    ctrl.setValue('2022/01/01');
    expect(ctrl.hasError('dateBefore')).toBeTrue();
  });

  it('should return null if input is an invalid date', () => {
    ctrl.setValue('20xe2/01p/01');
    expect(ctrl.getError('dateBefore')).toBeNull();
  });

  it('should return null if input is an invalid date', () => {
    ctrl.setValue('20/01/01');
    expect(ctrl.getError('dateBefore')).toBeNull();
  });

  it('should return null if input is an invalid date', () => {
    ctrl.setValue('20/01/2001');
    expect(ctrl.getError('dateBefore')).toBeNull();
  });

  it('should not contain error if no date is supplied', () => {
    ctrl.setValue(null);
    expect(ctrl.hasError('dateBefore')).toBeFalse();
  });
});

describe('Date After Validator', () => {

  const ctrl = new FormControl(null, RfxValidators.dateAfter('2010/01/01'));
  const ctrl2 = new FormControl(null, RfxValidators.dateAfter('2009/01/01', 'User defined message test'));
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        ReactiveFormsModule
      ]
    });
  });

  it('should return an error if the input date is less than the specified date.', () => {
    ctrl.setValue('2008/01/01');
    expect(ctrl.hasError('dateAfter')).toBeTrue();
  });

  it('should not contain error if the input date is less than specified date.', () => {
    ctrl.setValue('2022/01/01');
    expect(ctrl.hasError('dateAfter')).toBeFalse();
  });

  it('should contain default error message if default error message has not been overriden.', () => {
    ctrl.setValue('2008/01/01');
    const prettifiedMaxDate = new Date('2010/01/01').toLocaleDateString(undefined, { year: 'numeric', month: 'long', day: 'numeric' });
    expect(ctrl.getError('dateAfter')).toEqual(`Date entered must be greater than ${prettifiedMaxDate}.`);
  });

  it('should contain user supplied error message if the input date is less the specified date.', () => {
    ctrl2.setValue('2008/01/01');
    expect(ctrl2.getError('dateAfter')).toEqual('User defined message test');
  });

  it('should contain user supplied error message if the input date is less the specified date.', () => {
    ctrl.setValue('2010/01/01');
    expect(ctrl.hasError('dateAfter')).toBeTrue();
  });

  it('should return null if input is an invalid date".', () => {
    ctrl.setValue('20ec08/01-@/01');
    expect(ctrl.getError('dateAfter')).toBeNull();
  });

  it('should return null if input is an invalid date".', () => {
    ctrl.setValue('20/0/2021');
    expect(ctrl.getError('dateAfter')).toBeNull();
  });

  it('should return null if input is an invalid date".', () => {
    ctrl.setValue('20/0/202');
    expect(ctrl.getError('dateAfter')).toBeNull();
  });

  it('should not contain error if no date is supplied', () => {
    ctrl.setValue(null);
    expect(ctrl.hasError('dateAfter')).toBeFalse();
  });
});

describe('Password Complexity Validator', () => {

  const ctrl = new FormControl(null, RfxValidators.passwordComplexity());
  const ctrl2 = new FormControl(null, RfxValidators.passwordComplexity('helloworld'));
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        ReactiveFormsModule
      ]
    });
  });

  it('should return an error if the input string does not contain atleast 1 lowercase.', () => {
    ctrl.setValue('ABCDE');
    expect(ctrl.hasError('passwordComplexity')).toBeTrue();
  });

  it('should return an error if the input string does not contain atleast 1 digit.', () => {
    ctrl.setValue('abcde');
    expect(ctrl.hasError('passwordComplexity')).toBeTrue();
  });

  it('should return an error if the input string does not contain atleast 1 uppercase.', () => {
    ctrl.setValue('abcde');
    expect(ctrl.hasError('passwordComplexity')).toBeTrue();
  });

  it('should return an error if the input string does not contain atleast 1 special character.', () => {
    ctrl.setValue('abcde');
    expect(ctrl.hasError('passwordComplexity')).toBeTrue();
  });

  it('should contain the default error message if not custom message is supplied.', () => {
    ctrl.setValue('abcdeDN(((+');
    expect(ctrl.getError('passwordComplexity')).toEqual('Password must contain atleast 1 lowercase, uppercase, digit and a special character.');
  });

  it('should contain the custom error message if supplied.', () => {
    ctrl2.setValue('abcdeDN(((+');
    expect(ctrl2.getError('passwordComplexity')).toEqual('helloworld');
  });

  it('should not contain an error when the input string has atleast 1 lowercase, uppercase, special character and digit.', () => {
    ctrl.setValue('abcdeDN(((+8');
    expect(ctrl.hasError('passwordComplexity')).toBeFalse();
  });

});