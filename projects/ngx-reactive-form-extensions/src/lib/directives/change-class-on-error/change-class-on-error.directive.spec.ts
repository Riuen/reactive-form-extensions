import {Component, ElementRef, ViewChild} from "@angular/core";
import {ComponentFixture, TestBed} from "@angular/core/testing";
import {FormControl, FormGroup, ReactiveFormsModule, Validators} from "@angular/forms";
import {RFX_CONFIG} from "../../config/rfx-config";
import {ChangeClassOnErrorDirective} from './change-class-on-error.directive';
import {wait} from '../../utils/utilities';
import {appendFormControlError, removeFormControlError} from '../../utils/rfx-utils';

@Component({
  template: `
  <br/>
  <form [formGroup]="form">
    <input type="text" formControlName="f1" changeClassOnError #f1 />
    <input type="text" formControlName="f2" changeClassOnError validationTriggerCondition="dirty" #f2 />
    <input type="text" formControlName="f3" changeClassOnError errorClass="aqua-blue" #f3 />
    <input type="text" formControlName="f4" changeClassOnError class="unit-test" #f4 />
    <input type="text" formControlName="f5" changeClassOnError class="unit-test" validationTriggerCondition="dirty" #f5 />
    <input type="text" formControlName="f6" changeClassOnError class="unit-test" errorClass="aqua-blue style with space" #f6 />
    <input type="text" formControlName="f7" changeClassOnError #f7 />
  </form>
  `
})
class ChangeClassOnErrorDirectiveTestComponent {
  @ViewChild('f1') f1!: ElementRef<HTMLInputElement>;
  @ViewChild('f2') f2!: ElementRef<HTMLInputElement>;
  @ViewChild('f3') f3!: ElementRef<HTMLInputElement>;
  @ViewChild('f4') f4!: ElementRef<HTMLInputElement>;
  @ViewChild('f5') f5!: ElementRef<HTMLInputElement>;
  @ViewChild('f6') f6!: ElementRef<HTMLInputElement>;
  @ViewChild('f7') f7!: ElementRef<HTMLInputElement>;

  form = new FormGroup({
    f1: new FormControl(null, Validators.required),
    f2: new FormControl(null, Validators.required),
    f3: new FormControl(null, Validators.required),
    f4: new FormControl(null, Validators.required),
    f5: new FormControl(null, Validators.required),
    f6: new FormControl(null, Validators.required),
    f7: new FormControl(null, [Validators.required, Validators.email])
  });
}

describe('Change Class On Error Extension', () => {
  let component: ChangeClassOnErrorDirectiveTestComponent;
  let fixture: ComponentFixture<ChangeClassOnErrorDirectiveTestComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ChangeClassOnErrorDirectiveTestComponent],
      imports: [
        ReactiveFormsModule,
        ChangeClassOnErrorDirective
      ],
      providers: [
        {
          provide: RFX_CONFIG,
          useValue: {
            changeClassOnError: { cssClass: 'is-invalid'}
          }
        }
      ]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ChangeClassOnErrorDirectiveTestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should add css class when validation rule is broken.', async() => {
    component.form.markAllAsTouched();
    fixture.detectChanges();
    await wait(100);
    expect(component.f1.nativeElement.className).toContain('is-invalid');
  });

  it('should remove css class when no validation rules are broken.', () => {
    component.form.markAllAsTouched();
    expect(component.f1.nativeElement.className).not.toContain('is-invalid');
  });

  it('should successfully change the validation event when overriden. (trigger wrong event) ', () => {
    component.form.markAllAsTouched();
    expect(component.f2.nativeElement.className).not.toContain('is-invalid');
  });

  it('should successfully change the validation event when overridden.(trigger correct event)', async () => {
    component.form.get('f2').markAsDirty();
    fixture.detectChanges();
    await wait(100);
    expect(component.f2.nativeElement.className).toContain('is-invalid');
  });

  it('should be able to override global css class if "errorClass" property is assigned directly. (add override class)', async() => {
    component.form.markAllAsTouched();
    fixture.detectChanges();
    await wait(100);
    expect(component.f3.nativeElement.className).toContain('aqua-blue');
  });

  it('should be able to override global css class if "errorClass" property is assigned directly. (not contain global class)', () => {
    component.form.markAllAsTouched();
    fixture.detectChanges();
    expect(component.f3.nativeElement.className).not.toContain('is-invalid');
  });

  it('should preserve whatever css classes exist on the host', async() => {
    component.form.markAllAsTouched();
    fixture.detectChanges();
    await wait(100);
    const classListPreserved = component.f4.nativeElement.className.includes('unit-test') && component.f4.nativeElement.className.includes('is-invalid');
    expect(classListPreserved).toBeTrue();
  })

  it('should preserve whatever css classes exist on the host when triggerCondition is dirty',async () => {
    component.form.get('f5').markAsDirty();
    fixture.detectChanges();
    await wait(100);
    const classListPreserved = component.f5.nativeElement.className.includes('unit-test') && component.f5.nativeElement.className.includes('is-invalid');
    expect(classListPreserved).toBeTrue();
  });

  it('should preserve whatever css classes exist on the host when triggerCondition is touched',async () => {
    component.form.markAllAsTouched();
    fixture.detectChanges();
    await wait(100);
    const classListPreserved = component.f4.nativeElement.className.includes('unit-test') && component.f4.nativeElement.className.includes('is-invalid');
    expect(classListPreserved).toBeTrue();
  })

  it('should remove only the error class if the form was reset when triggerCondition is dirty',async () => {
    component.form.get('f5').markAsDirty();
    fixture.detectChanges();
    await wait(100);
    component.form.reset();
    expect(component.f5.nativeElement.className).not.toContain('is-invalid');
  })

  it('should remove only the error class if the form was reset when triggerCondition is touched',async () => {
    component.form.markAllAsTouched();
    fixture.detectChanges();
    await wait(100);
    component.form.reset();
    expect(component.f4.nativeElement.className).not.toContain('is-invalid');
  });

  it('should be able to apply class strings that contain multiple words seperated by spaces', async() => {
    component.form.markAllAsTouched();
    fixture.detectChanges();
    await wait(100);
    expect(component.f6.nativeElement.className).toContain('style with space');
  });

  it('should add error css class to field when form errors are manually added', async () => {
    component.form.get('f7').setValue('helloworld@email.com')
    component.form.get('f7').markAsTouched();
    appendFormControlError(component.form.get('f7'), 'manualError', 'Error added manually.');
    await wait(350);
    expect(component.f7.nativeElement.className).toContain('is-invalid');
  });

  it('should remove error css class from field when form errors are manually removed', async () => {
    component.form.get('f7').setValue('helloworld@email.com')
    component.form.get('f7').markAsTouched();
    appendFormControlError(component.form.get('f7'), 'manualError', 'Error added manually.');
    component.form.get('f7').updateValueAndValidity()
    await wait(350);
    removeFormControlError(component.form.get('f7'), 'manualError')
    component.form.get('f7').updateValueAndValidity()
    await wait(350);
    expect(component.f7.nativeElement.className).not.toContain('is-invalid');
  });
});

