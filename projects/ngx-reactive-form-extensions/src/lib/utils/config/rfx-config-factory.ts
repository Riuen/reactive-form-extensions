import {RfxConfig} from "../../config/rfx-config";
import {isString} from '../utilities';

export class RfxConfigBuilder {

  private readonly _config: RfxConfig;

  constructor() {
    this._config = {
      showValidationMessages: {
        cssClass: '',
        validationTriggerCondition: 'touched'
      },
      trim: {
        setEmptyValuesToNull: false
      },
      changeClassOnError: {
        cssClass: '',
        validationTriggerCondition: 'touched'
      }
    }
  }

  addTrimConfig(options): RfxConfigBuilder {
    if (typeof options?.setEmptyValuesToNull === 'boolean') {
      this._config.trim = { setEmptyValuesToNull: options.setEmptyValuesToNull};
      return this;
    }
    this._config.trim = {setEmptyValuesToNull: false};
    return this;
  }

  addValidationMessageConfig(options): RfxConfigBuilder {
    this._config.showValidationMessages = {
      cssClass: isString(options?.cssClass) ? options.cssClass : '',
      validationTriggerCondition: ['touched','dirty'].includes(options?.validationTriggerCondition)
      ? options.validationTriggerCondition : 'touched'
    }
    return this;
  }

  addClassChangeConfig(options): RfxConfigBuilder {
    this._config.changeClassOnError = {
      cssClass: isString(options?.cssClass) ? options.cssClass : '',
      validationTriggerCondition: ['touched','dirty'].includes(options?.validationTriggerCondition)
        ? options.validationTriggerCondition : 'touched'
    }
    return this;
  }

  getConfig(): RfxConfig {
    return this._config;
  }
}
