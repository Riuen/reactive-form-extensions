import { Directive, ElementRef, HostListener, Input, OnInit, Optional } from '@angular/core';
import { NgControl } from '@angular/forms';
import { RfxConfigService } from '../../rfx-config.service';
import { isEmpty, isString } from '../../utils/utilities';

@Directive({
  selector: '[trim]',
  standalone: true
})
export class TrimDirective implements OnInit {

  private _hostElementType: string;
  private _setEmptyValuesToNull: boolean;
  private _isEligibleForTrim: boolean;

  @Input() set setEmptyToNull(option: boolean | string) {
    this._setEmptyValuesToNull = (option == 'true');
  }

  constructor(
    private _configSvc: RfxConfigService,
    private _hostElement: ElementRef,
    @Optional() private _fieldControl: NgControl) { }

  ngOnInit(): void {
    this._hostElementType = this._hostElement.nativeElement.getAttribute('type');
    this._setEmptyValuesToNull = this._setEmptyValuesToNull ?? this._configSvc.getConfig().trim.setEmptyValuesToNull;
    this.initTrimEligibility();
  }

  @HostListener('blur')
  public onBlur(): void {
    const input = this._fieldControl?.value;
    if (!this._isEligibleForTrim || !isString(input)) {
      return;
    }

    let formattedInput = input?.trim();
    if (this._setEmptyValuesToNull) {
      formattedInput = isEmpty(formattedInput) ? null : formattedInput;
    }

    if (input?.length === formattedInput?.length) {
      return;
    }

    const errors = this._fieldControl.errors;
    this._fieldControl.control.setValue(formattedInput);
    this._fieldControl.control.setErrors(errors, {emitEvent: false});
  }

  private initTrimEligibility(): void {
    this._isEligibleForTrim = !this._hostElementType || (['text', 'password', 'email', 'tel', 'search', 'url'].includes(this._hostElementType))
  }
}
