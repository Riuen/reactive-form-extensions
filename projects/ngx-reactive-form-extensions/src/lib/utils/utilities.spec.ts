import {extractValidationMessageFromControl, isString} from "./utilities";
import {FormControl, Validators} from '@angular/forms';
import {appendFormControlError} from './rfx-utils';
import {ValidatorId} from './constants';

describe('Utilities - isString', () => {

  it('should correctly detect when the input is a string', () => {
    expect(isString('helloworld')).toBeTrue();
  });

  it('should correctly detect that an array is not a string', () => {
    expect(isString([])).toBeFalse();
  });

  it('should correctly detect that an object is not a string', () => {
    expect(isString({id: 'hi'})).toBeFalse();
  });

  it('should correctly detect that a number is not a string', () => {
    expect(isString(42)).toBeFalse();
  });

  it('should correctly detect that a date is not a string', () => {
    expect(isString(new Date())).toBeFalse();
  });

  it('should correctly detect that a boolean is not a string', () => {
    expect(isString(true)).toBeFalse();
  });
});

describe('Utilities - extractValidationMessageFromControl', () => {
  it('should extract custom error messages from custom error object', () => {
    const fc = new FormControl(null);
    appendFormControlError(fc, 'customError', 'This is a custom error');
    const customErrMsg = extractValidationMessageFromControl('customError', fc as any);
    expect(customErrMsg).toEqual('This is a custom error');
  });

  it('should extract default error message for Validators.required', () => {
    const fc = new FormControl('init', Validators.required);
    fc.setValue(null);
    const customErrMsg = extractValidationMessageFromControl(ValidatorId.Required, fc as any);
    expect(customErrMsg).toEqual('This field is required.');
  });

  it('should extract default error message for Validators.maxLength', () => {
    const fc = new FormControl(null, Validators.maxLength(3));
    fc.setValue('foobar');
    const customErrMsg = extractValidationMessageFromControl(ValidatorId.MaxLength, fc as any);
    expect(customErrMsg).toEqual('Value entered must be less than or equal to 3 characters.');
  });

  it('should extract default error message for Validators.minLength', () => {
    const fc = new FormControl(null, Validators.minLength(4));
    fc.setValue('foo');
    const customErrMsg = extractValidationMessageFromControl(ValidatorId.MinLength, fc as any);
    expect(customErrMsg).toEqual('Value entered must be greater than or equal to 4 characters.');
  });

  it('should extract default error message for Validators.min', () => {
    const fc = new FormControl(null, Validators.min(4));
    fc.setValue(3);
    const customErrMsg = extractValidationMessageFromControl(ValidatorId.Min, fc as any);
    expect(customErrMsg).toEqual('Value must be greater than or equal to 4.');
  });

  it('should extract default error message for Validators.max', () => {
    const fc = new FormControl(null, Validators.max(4));
    fc.setValue(5);
    const customErrMsg = extractValidationMessageFromControl(ValidatorId.Max, fc as any);
    expect(customErrMsg).toEqual('Value must be less than or equal to 4.');
  });

  it('should extract default error message for Validators.pattern', () => {
    const fc = new FormControl(null, Validators.pattern(/[a-z]/));
    fc.setValue(5);
    const customErrMsg = extractValidationMessageFromControl(ValidatorId.Pattern, fc as any);
    expect(customErrMsg).toEqual('Invalid input.');
  });

  it('should extract default error message for Validators.email', () => {
    const fc = new FormControl(null, Validators.email);
    fc.setValue('foobar');
    const customErrMsg = extractValidationMessageFromControl(ValidatorId.Email, fc as any);
    expect(customErrMsg).toEqual('Invalid email address.');
  });
});
