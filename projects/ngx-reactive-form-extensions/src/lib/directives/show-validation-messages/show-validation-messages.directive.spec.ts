import {Component, ElementRef, ViewChild} from '@angular/core';
import {ComponentFixture, TestBed} from '@angular/core/testing';
import {FormControl, FormGroup, ReactiveFormsModule, Validators} from '@angular/forms';
import {RfxValidators} from '../../validators/rfx-validators';
import {ShowValidationMessagesDirective} from './show-validation-messages.directive';
import {wait} from '../../utils/utilities';
import {appendFormControlError, removeFormControlError} from '../../utils/rfx-utils';

@Component({
  template: `
  <br/>
  <form [formGroup]="form">
    <input type="text" formControlName="f1" showValidationMessages #f1 />
    <input type="text" formControlName="f2" showValidationMessages #f2 />
    <input type="text" formControlName="f3" showValidationMessages #f3 />
    <input type="text" formControlName="f4" showValidationMessages validationTriggerCondition="dirty" #f4 />
    <input type="text" formControlName="f5" showValidationMessages #f5 />
    <input type="text" formControlName="f6" showValidationMessages #f6 />
  </form>
  `
})
class DisplayValidationMessagesDirectiveTestComponent {
  @ViewChild('f1', {static: true}) f1!: ElementRef<HTMLInputElement>;
  @ViewChild('f2') f2!: ElementRef<HTMLInputElement>;
  @ViewChild('f3') f3!: ElementRef<HTMLInputElement>;
  @ViewChild('f4') f4!: ElementRef<HTMLInputElement>;
  @ViewChild('f5') f5!: ElementRef<HTMLInputElement>;
  @ViewChild('f6') f6!: ElementRef<HTMLInputElement>;

  form = new FormGroup({
    f1: new FormControl(null, Validators.required),
    f2: new FormControl(null, RfxValidators.required('F2 is required.')),
    f3: new FormControl(null, RfxValidators.required()),
    f4: new FormControl(null, Validators.required),
    f5: new FormControl(null),
    f6: new FormControl(null, [RfxValidators.required('f6 required'), RfxValidators.maxLength(5, 'maxlength5')])
  });
}

describe('Validation Message Display Extension', () => {
  let component: DisplayValidationMessagesDirectiveTestComponent;
  let fixture: ComponentFixture<DisplayValidationMessagesDirectiveTestComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [DisplayValidationMessagesDirectiveTestComponent],
      imports: [
        ReactiveFormsModule,
        ShowValidationMessagesDirective
      ]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DisplayValidationMessagesDirectiveTestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should display the default validation message when the validation rule is violated.',(done) => {
    component.form.get('f1').markAsTouched();
    const defaultErrorMsg = 'This field is required.';

    setTimeout(() => {
      fixture.detectChanges();
      const fieldErrorMsg: string = component.f1.nativeElement.nextElementSibling.textContent.trim();
      expect(fieldErrorMsg).toEqual(defaultErrorMsg);
      done();
    }, 350);
  });


  it('should display no validation message when no validation rules are broken.', (done) => {

    component.form.get('f1').setValue('helloworld');
    component.form.markAllAsTouched();

    setTimeout(() => {
      fixture.detectChanges();
      const fieldErrorMsg: string = component.f1.nativeElement.nextElementSibling.textContent.trim();
      expect(fieldErrorMsg).toBeFalsy();
      done();
    }, 350);
  });

  it('should add user defined error message when validation rule is broken.', (done) => {
    component.form.get('f2').setValue(null);
    component.form.get('f2').markAsTouched();
    setTimeout(() => {
      fixture.detectChanges();
      expect(component.f2.nativeElement.nextElementSibling.textContent.trim()).toContain('F2 is required.');
      done();
    }, 350);
  });

  it('should add default error message when the no user defined error message is set.', (done) => {

    component.form.get('f3').setValue(null);
    component.form.get('f3').markAsTouched();
    setTimeout(() => {
      fixture.detectChanges();
      expect(component.f3.nativeElement.nextElementSibling.textContent.trim()).toContain('This field is required.');
      done();
    }, 350);
  });

  it('should successfully change the validation condition when overriden. (trigger wrong condition) ', () => {
    component.form.markAllAsTouched();
    expect(component.f4.nativeElement.className).not.toContain('is-invalid');
  });

  it('should successfully change the validation condition when overridden. (correct condition) ', async () => {
    component.form.get('f4').setValue(null);
    component.form.get('f4').markAsDirty();
    await wait(350);
    fixture.detectChanges();
    expect(component.f4.nativeElement.nextElementSibling.textContent.trim()).toContain('This field is required.');
  });

  it('should hide the error message if the form was reset when triggerCondition is touched', async () => {
    component.form.markAllAsTouched();
    await wait(350);
    fixture.detectChanges();
    component.form.reset();
    await wait(400);
    expect(component.f3.nativeElement.nextElementSibling.textContent.trim()).not.toContain('This field is required.');
  });

  it('should hide the error message if the form was reset when triggerCondition is dirty', async () => {
    component.form.get('f4').markAsDirty();
    await wait(350);
    fixture.detectChanges();
    component.form.reset();
    await wait(350);
    expect(component.f4.nativeElement.nextElementSibling.textContent.trim()).not.toContain('This field is required.');
  });

  it('should display form errors that are manually added', async () => {
    component.form.get('f5').markAsTouched();
    appendFormControlError(component.form.get('f5'), 'manualError', 'Error added manually.');
    await wait(350);
    expect(component.f5.nativeElement.nextElementSibling.textContent.trim()).toContain('Error added manually.');
  });

  it('should remove form errors that are manually removed', async () => {
    component.form.get('f5').markAsTouched();
    appendFormControlError(component.form.get('f5'), 'manualError', 'Error added manually.');
    component.form.get('f5').updateValueAndValidity()
    await wait(350);
    removeFormControlError(component.form.get('f5'), 'manualError');
    component.form.get('f5').updateValueAndValidity()
    expect(component.f5.nativeElement.nextElementSibling.textContent.trim()).not.toContain('Error added manually.');
  });

  it('should update to reflect the latest error detected on the field', async () => {
    component.form.get('f6').markAsTouched();

    fixture.detectChanges();
    await wait(400);
    let fieldErrorMsg: string = component.f6.nativeElement.nextElementSibling.textContent.trim();
    const isError1Set = fieldErrorMsg === 'f6 required';

    component.form.get('f6').setValue('maxlengthexceed');
    fixture.detectChanges();
    await wait(400);
    fieldErrorMsg = component.f6.nativeElement.nextElementSibling.textContent.trim();
    const isError2Set = fieldErrorMsg === 'maxlength5';

    expect(isError1Set && isError2Set).toBeTrue();
  });
});
