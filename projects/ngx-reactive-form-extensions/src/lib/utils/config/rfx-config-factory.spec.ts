import {RfxConfigBuilder} from './rfx-config-factory';

describe('Config Builder', () => {
  it('should only mutate showValidationMessage.cssClass', () => {
    const config = new RfxConfigBuilder().addValidationMessageConfig({cssClass: 'foo'}).getConfig();
    expect(config).toEqual({showValidationMessages: {cssClass: 'foo', validationTriggerCondition: 'touched'}, trim: {setEmptyValuesToNull: false}, changeClassOnError: {cssClass: '', validationTriggerCondition: 'touched'}});
  });

  it('should only mutate showValidationMessage.cssClass if css class is a valid string', () => {
    const config = new RfxConfigBuilder().addValidationMessageConfig({cssClass: 1}).getConfig();
    expect(config).toEqual({showValidationMessages: {cssClass: '', validationTriggerCondition: 'touched'}, trim: {setEmptyValuesToNull: false}, changeClassOnError: {cssClass: '', validationTriggerCondition: 'touched'}});
  });

  it('should only mutate showValidationMessage.cssClass if css class is a valid string', () => {
    const config = new RfxConfigBuilder().addValidationMessageConfig({cssClass: null}).getConfig();
    expect(config).toEqual({showValidationMessages: {cssClass: '', validationTriggerCondition: 'touched'}, trim: {setEmptyValuesToNull: false}, changeClassOnError: {cssClass: '', validationTriggerCondition: 'touched'}});
  });

  it('should only mutate showValidationMessage.cssClass if css class is a valid string', () => {
    const config = new RfxConfigBuilder().addValidationMessageConfig({foo: 'im valid'}).getConfig();
    expect(config).toEqual({showValidationMessages: {cssClass: '', validationTriggerCondition: 'touched'}, trim: {setEmptyValuesToNull: false}, changeClassOnError: {cssClass: '', validationTriggerCondition: 'touched'}});
  });

  it('should only mutate showValidationMessage.validationTriggerCondition', () => {
    const config = new RfxConfigBuilder().addValidationMessageConfig({validationTriggerCondition: 'dirty'}).getConfig();
    expect(config).toEqual({showValidationMessages: {cssClass: '', validationTriggerCondition: 'dirty'}, trim: {setEmptyValuesToNull: false}, changeClassOnError: {cssClass: '', validationTriggerCondition: 'touched'}});
  });

  it('should not mutate showValidationMessage.validationTriggerCondition if invalid trigger condition is passed', () => {
    const config = new RfxConfigBuilder().addValidationMessageConfig({validationTriggerCondition: 'foo'}).getConfig();
    expect(config).toEqual({showValidationMessages: {cssClass: '', validationTriggerCondition: 'touched'}, trim: {setEmptyValuesToNull: false}, changeClassOnError: {cssClass: '', validationTriggerCondition: 'touched'}});
  });

  it('should not apply invalid trim options', () => {
    const config = new RfxConfigBuilder().addTrimConfig({foo: 'bar'}).getConfig();
    expect(config).toEqual({showValidationMessages: {cssClass: '', validationTriggerCondition: 'touched'}, trim: {setEmptyValuesToNull: false}, changeClassOnError: {cssClass: '', validationTriggerCondition: 'touched'}});
  });

  it('should not apply invalid trim options', () => {
    const config = new RfxConfigBuilder().addTrimConfig({setEmptyValuesToNull: 'foobar'}).getConfig();
    expect(config).toEqual({showValidationMessages: {cssClass: '', validationTriggerCondition: 'touched'}, trim: {setEmptyValuesToNull: false}, changeClassOnError: {cssClass: '', validationTriggerCondition: 'touched'}});
  });

  it('should only mutate changeClassOnError.cssClass', () => {
    const config = new RfxConfigBuilder().addClassChangeConfig({cssClass: 'foo'}).getConfig();
    expect(config).toEqual({showValidationMessages: {cssClass: '', validationTriggerCondition: 'touched'}, trim: {setEmptyValuesToNull: false}, changeClassOnError: {cssClass: 'foo', validationTriggerCondition: 'touched'}});
  });

  it('should only mutate changeClassOnError.cssClass if css class is a valid string', () => {
    const config = new RfxConfigBuilder().addClassChangeConfig({cssClass: []}).getConfig();
    expect(config).toEqual({showValidationMessages: {cssClass: '', validationTriggerCondition: 'touched'}, trim: {setEmptyValuesToNull: false}, changeClassOnError: {cssClass: '', validationTriggerCondition: 'touched'}});
  });

  it('should only mutate showValidationMessage.validationTriggerCondition', () => {
    const config = new RfxConfigBuilder().addClassChangeConfig({validationTriggerCondition: 'dirty'}).getConfig();
    expect(config).toEqual({showValidationMessages: {cssClass: '', validationTriggerCondition: 'touched'}, trim: {setEmptyValuesToNull: false}, changeClassOnError: {cssClass: '', validationTriggerCondition: 'dirty'}});
  });

  it('should only mutate showValidationMessage.validationTriggerCondition if invalid trigger condition is passed', () => {
    const config = new RfxConfigBuilder().addClassChangeConfig({validationTriggerCondition: 'foo'}).getConfig();
    expect(config).toEqual({showValidationMessages: {cssClass: '', validationTriggerCondition: 'touched'}, trim: {setEmptyValuesToNull: false}, changeClassOnError: {cssClass: '', validationTriggerCondition: 'touched'}});
  });

  it('should only mutate showValidationMessage.validationTriggerCondition if invalid trigger condition is passed', () => {
    const config = new RfxConfigBuilder().addClassChangeConfig({validationTriggerCondition: {foo: 'bar'}}).getConfig();
    expect(config).toEqual({showValidationMessages: {cssClass: '', validationTriggerCondition: 'touched'}, trim: {setEmptyValuesToNull: false}, changeClassOnError: {cssClass: '', validationTriggerCondition: 'touched'}});
  });
});
