import {ChangeDetectionStrategy, ChangeDetectorRef, Component, Input} from '@angular/core';
import {CommonModule} from '@angular/common';

/**
 * Input:
 *  - Array consisting of error names and associated error messages
 * Purpose:
 *  This component will render the relevant error message based on the errors
 *  present in the form control.
 */

 @Component({
  selector: 'lib-field-validation-message-view',
  template: `
    @if (message) {
      <div [class]="fieldClass">{{message}}</div>
    }
  `,
   styles: [],
   standalone: true,
   imports: [CommonModule],
   changeDetection: ChangeDetectionStrategy.OnPush
})
export class FieldValidationMessageViewComponent {

  @Input() message: string;
  @Input() fieldClass: string;

  constructor(private _cdr:ChangeDetectorRef) {}

  detectChanges(): void {
    this._cdr.detectChanges();
  }
}
