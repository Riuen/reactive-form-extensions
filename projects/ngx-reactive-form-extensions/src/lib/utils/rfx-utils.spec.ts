import { TestBed } from "@angular/core/testing";
import { FormControl, ReactiveFormsModule, Validators } from "@angular/forms";
import { appendFormControlError, removeFormControlError } from "./rfx-utils";

describe('Reactive form utils - removeFormControlError', () => {

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        ReactiveFormsModule
      ]
    });
  });

  it('should remove only the error specified', () => {
    const ctrl = new FormControl('a', [Validators.minLength(8), Validators.email]);
    removeFormControlError(ctrl, 'email');
    expect(!ctrl.hasError('email') && (Object.keys(ctrl.errors).length === 1)).toBeTrue();
  });

  it('should not remove any errors if the specified error is not found', () => {
    const ctrl = new FormControl('a', [Validators.minLength(8), Validators.email]);
    removeFormControlError(ctrl, 'max');
    expect(ctrl.hasError('email') && ctrl.hasError('minlength') && (Object.keys(ctrl.errors).length === 2)).toBeTrue();
  });

  it('should not remove any errors if falsy value supplied as error name', () => {
    const ctrl = new FormControl('a', [Validators.minLength(8), Validators.email]);
    removeFormControlError(ctrl, null);
    expect(ctrl.hasError('email') && ctrl.hasError('minlength') && (Object.keys(ctrl.errors).length === 2)).toBeTrue();
  });

  it('should not remove any errors if falsy value supplied as control', () => {
    const ctrl = new FormControl('a', [Validators.minLength(8), Validators.email]);
    removeFormControlError(null, 'max');
    expect(ctrl.hasError('email') && ctrl.hasError('minlength') && (Object.keys(ctrl.errors).length === 2)).toBeTrue();
  });
});

describe('Reactive form utils - appendFormControlError', () => {

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        ReactiveFormsModule
      ]
    });
  });

  it('should append the error to the form control', () => {
    const ctrl = new FormControl('a', [Validators.minLength(8)]);
    appendFormControlError(ctrl, 'email', 'Invalid email');
    expect(ctrl.hasError('email') && ctrl.hasError('minlength') && (Object.keys(ctrl.errors).length === 2)).toBeTrue();
  });

  it('should not append any errors if no error name is supplied', () => {
    const ctrl = new FormControl('a', [Validators.minLength(8), Validators.email]);
    appendFormControlError(ctrl, '', 'Invalid email');
    expect(ctrl.hasError('email') && ctrl.hasError('minlength') && (Object.keys(ctrl.errors).length === 2)).toBeTrue();
  });

  it('should not append any errors if error already exists', () => {
    const ctrl = new FormControl('a', [Validators.minLength(8), Validators.email]);
    appendFormControlError(ctrl, 'email', 'Invalid email');
    expect(ctrl.hasError('email') && ctrl.hasError('minlength') && (Object.keys(ctrl.errors).length === 2)).toBeTrue();
  });

  it('should not append any errors if falsy value supplied as error name', () => {
    const ctrl = new FormControl('a', [Validators.minLength(8), Validators.email]);
    appendFormControlError(ctrl, null, 'Invalid input');
    expect(ctrl.hasError('email') && ctrl.hasError('minlength') && (Object.keys(ctrl.errors).length === 2)).toBeTrue();
  });

  it('should not append errors if falsy value supplied as control', () => {
    const ctrl = new FormControl('a', [Validators.minLength(8)]);
    appendFormControlError({} as any, 'email', 'Invalid input');
    expect(ctrl.hasError('minlength') && (Object.keys(ctrl.errors).length === 1)).toBeTrue();
  });

  it('should not append anything if null is passed as the control', () => {
    const ctrl = new FormControl('a', [Validators.minLength(8)]);
    appendFormControlError(null, 'email', 'Invalid email');
    expect(ctrl.hasError('minlength') && (Object.keys(ctrl.errors).length === 1)).toBeTrue();
  });
});