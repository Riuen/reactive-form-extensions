import {DestroyRef, Directive, ElementRef, inject, Input, OnInit, Optional, Renderer2} from '@angular/core';
import {NgControl} from '@angular/forms';
import {RfxConfigService} from '../../rfx-config.service';
import {combineLatest, distinctUntilChanged, filter, map, Observable, startWith} from 'rxjs';
import {takeUntilDestroyed} from '@angular/core/rxjs-interop';
import {watchElementClassChange} from '../../utils/utilities';

@Directive({
  selector: '[changeClassOnError]',
  standalone: true
})
export class ChangeClassOnErrorDirective implements OnInit {

  private _destroyRef = inject(DestroyRef);

  @Input() errorClass: string;
  @Input() validationTriggerCondition: 'touched' | 'dirty';

  constructor(
    private _configSvc: RfxConfigService,
    private _renderer2: Renderer2,
    private _elementRef: ElementRef,
    @Optional() private _fieldControl: NgControl) {
    this.errorClass = this._configSvc.getConfig().changeClassOnError.cssClass;
    this.validationTriggerCondition = this._configSvc.getConfig().changeClassOnError.validationTriggerCondition;
  }


  ngOnInit(): void {
    if (!this._fieldControl) {
      return;
    }
    combineLatest([
      this.getValidityClassChangeStream(),
      this.getFormResetEventStream()
    ]).pipe(takeUntilDestroyed(this._destroyRef))
      .pipe(map(() => ({
        invalid: this._fieldControl.invalid,
        isTriggerCondition: this.isControlInValidationTriggerState()
      })))
      .subscribe(state => {
        if (state.invalid && state.isTriggerCondition) {
          this.addErrorClass();
          return;
        }
        this.removeErrorClass();
      });
  }

  private addErrorClass(): void {
    this._renderer2.setAttribute(this._elementRef.nativeElement, 'class', `${this._elementRef.nativeElement.className} ${this.errorClass}`);
  }

  private removeErrorClass(): void {
    const originalClassName = this._elementRef.nativeElement.className.replace(this.errorClass, '');
    this._renderer2.setAttribute(this._elementRef.nativeElement, 'class', originalClassName);
  }

  private isControlInValidationTriggerState(): boolean {
    if (!this._fieldControl.pristine || this._fieldControl.value) {
      return true;
    }
    return this._fieldControl[this.validationTriggerCondition];
  }

  private getFormResetEventStream(): Observable<boolean> {
    return this._fieldControl.valueChanges
      .pipe(map(() => this._fieldControl.pristine))
      .pipe(filter((isPristine) => isPristine))
      .pipe(distinctUntilChanged())
      .pipe(startWith(true));
  }

  private getValidityClassChangeStream(): Observable<string> {
    return watchElementClassChange(this._elementRef)
      .pipe(distinctUntilChanged())
      .pipe(filter((className) => className.includes('ng-valid') || className.includes('ng-invalid')))
      .pipe(startWith(''));
  }
}
