import { NgControl } from "@angular/forms";
import {ValidatorId} from './constants';
import {Observable} from 'rxjs';
import {ElementRef} from '@angular/core';

/**
 * Returns the corresponding error message for the validator error.
 * @param errorName The name of the error.
 * @param control The form control.
 */
 export function extractValidationMessageFromControl(errorName: string, control: NgControl): string {
  switch (errorName) {
      case ValidatorId.Required: return 'This field is required.';
      case ValidatorId.MinLength: return `Value entered must be greater than or equal to ${control.getError(ValidatorId.MinLength)['requiredLength']} characters.`;
      case ValidatorId.MaxLength: return `Value entered must be less than or equal to ${control.getError(ValidatorId.MaxLength)['requiredLength']} characters.`;
      case ValidatorId.Pattern: return 'Invalid input.';
      case ValidatorId.Min: return `Value must be greater than or equal to ${control.getError(ValidatorId.Min)[ValidatorId.Min]}.`;
      case ValidatorId.Max: return `Value must be less than or equal to ${control.getError(ValidatorId.Max)[ValidatorId.Max]}.`;
      case ValidatorId.Email: return `Invalid email address.`;
      default: return isString(control.getError(errorName))
        ? control.getError(errorName)
        : 'Invalid input.';
  }
}

export function isString (input: any) {
  return (Object.prototype.toString.call(input) === '[object String]');
}

/**
 * Validates if the supplied date is valid.
 * @param date
 * @returns
 */
export function isValidDate(date: string | Date): boolean {
  if (!date) {
    return false;
  }
  return (date instanceof Date && !isNaN(date.getTime())) || (!isNaN(new Date(date).getTime()));
}

/**
 * Checks if the specified value is empty. A value is considered empty if meets any of the following criteria:
 * - Is an empty string
 * - Length is equal to 0 (if array)
 * - Is ```undefined``` || ```null```
 * - Is an empty object ```{}```
 * @param value The value to check
 * @returns true/false
 */
 export function isEmpty(value: any): boolean {

  if (value === null || value === undefined) {
    return true;
  }

  if (value instanceof Array) {
    return (value?.length === 0);
  }

  if (typeof value === 'string') {
    return (value?.trim().length === 0);
  }

  if (typeof value === 'object') {
    if (value instanceof Date || value instanceof File) {
      return false;
    }
    return Object.keys(value).length === 0;
  }

  return false;
}

export function isBefore(date: Date | number | string, fixedDate: Date | number | string): boolean {
  return startOfDay(date).getTime() < startOfDay(fixedDate).getTime();
}

export function isAfter(date: Date | number | string, fixedDate: Date | number | string): boolean {
  return startOfDay(date).getTime() > startOfDay(fixedDate).getTime();
}

function startOfDay(input: Date | number | string): Date {
  const date = toDate(input)
  date.setHours(0, 0, 0, 0)
  return date;
}

function toDate(input: Date | number | string): Date{
  const argStr = Object.prototype.toString.call(input)

  if (input instanceof Date || (typeof input === 'object' && argStr === '[object Date]')) {
    return new Date(input.getTime())
  } else if (typeof input === 'number' || argStr === '[object Number]' || typeof input === 'string') {
    return new Date(input);
  } else {
    return new Date(NaN);
  }
}

export async function wait(timeout: number) {
  return await new Promise(resolve => setTimeout(() => resolve(true), timeout))
}

export function watchElementClassChange(elementRef: ElementRef): Observable<string> {
   let previousValue = elementRef.nativeElement.className;
  return new Observable<string>(subscriber => {
    const observer = new MutationObserver((mutations: MutationRecord[]) => {
      mutations.forEach(() => {
        if (previousValue !== elementRef.nativeElement.className) {
          subscriber.next(elementRef.nativeElement.className)
        }
        previousValue = elementRef.nativeElement.className;
      });
    });
    observer.observe(elementRef.nativeElement, {
      attributeFilter: ['class']
    });

    return {
      unsubscribe: function(){
        observer.disconnect();
      }
    }
  })
}
