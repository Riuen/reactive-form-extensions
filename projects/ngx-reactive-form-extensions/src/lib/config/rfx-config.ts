import { InjectionToken } from "@angular/core";

export type RfxConfig = {
  showValidationMessages?: {
    cssClass?: string;
    validationTriggerCondition?: 'touched' | 'dirty'
  }
  trim?: {
    setEmptyValuesToNull: boolean
  }
  changeClassOnError?: {
    cssClass?: string;
    validationTriggerCondition?: 'touched' | 'dirty'
  }
}

export const RFX_CONFIG = new InjectionToken<RfxConfig>('RFX_CONFIG');
