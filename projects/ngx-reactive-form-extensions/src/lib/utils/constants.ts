export const ValidatorId = {
  Required: 'required',
  MinLength: 'minlength',
  MaxLength: 'maxlength',
  Pattern: 'pattern',
  Min: 'min',
  Max: 'max',
  Email: 'email',
  DateBefore: 'dateBefore',
  DateAfter: 'dateAfter',
  FieldMatch: 'fieldMatch',
  PasswordComplexity: 'passwordComplexity'
} as const;

type ObjectValues<T> = T[keyof T];
export type ValidatorId = ObjectValues<typeof ValidatorId>;
