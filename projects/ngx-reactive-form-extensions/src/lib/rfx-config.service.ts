import { Inject, Injectable, Optional } from '@angular/core';
import { RfxConfig, RFX_CONFIG } from './config/rfx-config';
import { RfxConfigBuilder } from './utils/config/rfx-config-factory';

@Injectable({
  providedIn: 'root'
})
export class RfxConfigService {

  private readonly _config: RfxConfig;

  constructor(@Optional() @Inject(RFX_CONFIG) private customOptions: RfxConfig) {
    const configBuilder = new RfxConfigBuilder();
    configBuilder.addValidationMessageConfig(this.customOptions?.showValidationMessages)
      .addClassChangeConfig(customOptions?.changeClassOnError)
      .addTrimConfig(customOptions?.trim);

    this._config = configBuilder.getConfig();
  }

  getConfig(): RfxConfig {
    return this._config;
  }
}
